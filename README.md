## Funcionalidades

#### Criação de usuário

* Requisito funcional:
  * Deve ser possível cadastrar um usuário na aplicação

* Regras de negócio:
  * O email deve ser único
  * Por padrão o saldo de um usuário deve ser zero
  * Por padrão o usuário deve ser criado como ativo
  * A senha do usuário deve ser criptografada

#### Criação de sessão

* Requisito funcional:
  * Deve ser possível para o usuário logar no sistema (autenticar-se)

* Regra de negócio:
  * O usuário deve fornecer credenciais corretas para criar uma nova sessão
  * A criação de sessão deve retornar um token jwt
  * Uma sessão deve expirar em 86400 segundos (24 horas)
  * O usuário deve estar ativo para criar uma sessão

#### Cadastro de endereço

* Requisito funcional:
  * Deve ser possível cadastar o endereço de um usuário na aplicação

* Regras de negócio:
  * Não deve ser possível cadastrar um endereço para um usuário não existente
  * O endereço deve ser validado
  * Um usuário pode ter somente um endereço
  * O usuário deve estar autenticado para cadastrar o seu endereço

* Requisitos não funcionais:
  * O endereço deeve ser validado através da api do ViaCEP

#### Atualização de endereço

* Requisito funcional:
  * Deve ser possível atualizar as informações de um endereço de um usuário

* Regras de negócio:
  * Não deve ser possível atualizar um endereço não existente
  * As novas informações devem ser validadas
  * O usuário deve estar autenticado para fazer a alteração

#### Listagem de endereço

* Requisito funcional:
  * Deve ser possível para o usuário visualizar o seu endereço

* Regra de negócio:
  * O usuário pode visualizar apenas o seu endereço

#### Criação de uma transação

* Requisito funcional:
  * Deve ser possível criar uma transação

* Regra de negócio:
  * As transações podem ser de três tipos: compra, depósito e saque (purchase, deposit, withdraw)
  * As transações devem ser feitas por um usuário cadastrado e ativo
  * Um usuário não pode fazer um saque ou uma compra com um saldo maior que possui
  * Uma transação não pode ser modificada ou excluída
  *

#### Listagem de transações

* Requisitos funcionais:
  * Deve ser possível listar todas as transações realizadas por um determinado usuário

# Executando a aplicação

1. Faça o clone do repositório com `git clone git@gitlab.com:abelsouzacosta/coins-backend-test.git`

2. Entre no diretório: `cd coins-backend-test`

3. Instale as dependências com `yarn` ou `npm install`

4. Renomeie o arquivo **.env.example** para **.env**

5. Essa aplicação faz uso de um banco de dados mysql, assim sendo é preciso que se tenha uma instância do MySQL na sua máquina, que pode ser obtida através dos seguintes comandos:

  * `docker pull mysql`
  * `docker run --name mysql -e "MYSQL_ROOT_PASSWORD=docker" -e "MYSQL_DATABASE=coins" -e "MYSQL_USER=docker" -e "MYSQL_PASSWORD=localhost" -p 3306:3306 -d mysql:latest`

6. Execute o container mysql: `docker run mysql`

7. Execute as migrations com: `yarn typeorm migration:run` ou `npm run typeorm migration:run`

8. Execute a aplicação com `yarn dev` ou `npm run dev`

A aplicação estará disponível em [http://localhost:3000](http://localhost:3000) e os endpoints do insomnia podem ser obtidos abaixo:

[![Run in Insomnia}](https://insomnia.rest/images/run.svg)](https://insomnia.rest/run/?label=Coins%20backend%20test&uri=https%3A%2F%2Fraw.githubusercontent.com%2Fabelsouzacosta%2Flibrary-content%2Fmaster%2Fcoins.json%3Ftoken%3DGHSAT0AAAAAABPOCJRWHNXDK5POCNEZ6WBQYRBOLJA)
