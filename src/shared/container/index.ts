import { TransactionRepository } from "modules/transactions/infra/typeorm/repositories/TransactionRepository";
import { ITransactionRepository } from "modules/transactions/repositories/ITransactionRepository";
import { AddressRepository } from "modules/users/infra/typeorm/repositories/AddressRepository";
import { UserRepository } from "modules/users/infra/typeorm/repositories/UserRepository";
import { IAddressRepository } from "modules/users/repositories/address/IAddressRepository";
import { IUserRepository } from "modules/users/repositories/users/IUserRepository";
import { AddressValidator } from "modules/users/utils/address/implementation/AddressValidator";
import { IAddressValidator } from "modules/users/utils/address/validator/IAddressValidator";
import { PasswordHandler } from "modules/users/utils/cryptography/implementations/PasswordHandler";
import { IPasswordHandler } from "modules/users/utils/cryptography/password/IPasswordHandler";
import { UserValidator } from "modules/users/utils/users/implementation/UserValidator";
import { IUserValidator } from "modules/users/utils/users/validator/IUserValidator";
import { container } from "tsyringe";

container.registerSingleton<IUserRepository>("UserRepository", UserRepository);

container.registerSingleton<IPasswordHandler>(
  "PasswordHandler",
  PasswordHandler
);

container.registerSingleton<IAddressRepository>(
  "AddressRepository",
  AddressRepository
);

container.registerSingleton<IAddressValidator>(
  "AddressValidator",
  AddressValidator
);

container.registerSingleton<IUserValidator>("UserValidator", UserValidator);

container.registerSingleton<ITransactionRepository>(
  "TransactionRepository",
  TransactionRepository
);
