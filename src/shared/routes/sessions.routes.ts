import { Router } from "express";
import { CreateSessionController } from "modules/users/usecases/users/createSession/CreateSessionController";

const sessionsRouter = Router();

const create = new CreateSessionController();

sessionsRouter.post("/", create.handle);

export { sessionsRouter };
