import { Router } from "express";
import { CreateTransactionController } from "modules/transactions/usecases/transactions/createTransaction/CreateTransactionController";
import { GetAllTransactionsController } from "modules/transactions/usecases/transactions/getAllTransactions/GetAllTransactionsController";
import isAuthenticated from "shared/infra/middlewares/AuthenticationMiddleware";

const transactionRouter = Router();

const create = new CreateTransactionController();
const list = new GetAllTransactionsController();

transactionRouter.use(isAuthenticated);

transactionRouter.get("/", list.handle);

transactionRouter.post("/", create.handle);

export { transactionRouter };
