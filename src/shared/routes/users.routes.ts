import { Router } from "express";
import { CreateUserController } from "modules/users/usecases/users/createUser/CreateUserController";
import { DisableUserController } from "modules/users/usecases/users/disableUser/DisableUserController";
import { ReactivateUserController } from "modules/users/usecases/users/reactivateUser/ReactivateUserController";
import isAuthenticated from "shared/infra/middlewares/AuthenticationMiddleware";

const userRouter = Router();

const create = new CreateUserController();
const disable = new DisableUserController();
const reactivate = new ReactivateUserController();

userRouter.post("/", create.handle);

userRouter.delete("/", isAuthenticated, disable.handle);

userRouter.patch("/", reactivate.handle);

export { userRouter };
