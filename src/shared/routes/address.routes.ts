import { Router } from "express";
import { CreateAddressController } from "modules/users/usecases/users/createAddress/CreateAddressController";
import { DeleteAddressController } from "modules/users/usecases/users/deleteAddress/DeleteAddressController";
import { GetUserAddressController } from "modules/users/usecases/users/getUserAddress/GetUserAddressController";
import { UpdateAddressController } from "modules/users/usecases/users/updateAddress/UpdateAddressController";
import isAuthenticated from "shared/infra/middlewares/AuthenticationMiddleware";

const addressRouter = Router();
const create = new CreateAddressController();
const update = new UpdateAddressController();
const list = new GetUserAddressController();
const remove = new DeleteAddressController();

addressRouter.use(isAuthenticated);

addressRouter.post("/", create.handle);

addressRouter.get("/", list.handle);

addressRouter.put("/:id", update.handle);

addressRouter.delete("/:id", remove.handle);

export { addressRouter };
