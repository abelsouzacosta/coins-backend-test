import { Router } from "express";

import { addressRouter } from "./address.routes";
import { sessionsRouter } from "./sessions.routes";
import { transactionRouter } from "./transactions.routes";
import { userRouter } from "./users.routes";

const router = Router();

router.use("/users", userRouter);
router.use("/address", addressRouter);
router.use("/sessions", sessionsRouter);
router.use("/transactions", transactionRouter);

export { router };
