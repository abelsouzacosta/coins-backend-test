import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey,
} from "typeorm";

export class AddColumnUserIdToAddressTable1646201057775
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    // adds column
    await queryRunner.addColumn(
      "address",
      new TableColumn({
        name: "user_id",
        type: "varchar",
        isNullable: false,
      })
    );

    // adds constraint
    await queryRunner.createForeignKey(
      "address",
      new TableForeignKey({
        name: "AddressUserId",
        columnNames: ["user_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "users",
        onDelete: "CASCADE",
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    // drops column
    await queryRunner.dropColumn("address", "user_id");
    // drops foreing key
    await queryRunner.dropForeignKey("address", "AddressUserId");
  }
}
