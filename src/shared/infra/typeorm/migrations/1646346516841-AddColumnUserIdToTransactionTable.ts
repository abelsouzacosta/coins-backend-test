import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey,
} from "typeorm";

export class AddColumnUserIdToTransactionTable1646346516841
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    // adds column
    await queryRunner.addColumn(
      "transactions",
      new TableColumn({
        name: "user_id",
        type: "varchar",
        isNullable: false,
      })
    );

    // adds constraint
    await queryRunner.createForeignKey(
      "transactions",
      new TableForeignKey({
        name: "TransactionUserId",
        columnNames: ["user_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "users",
        onDelete: "CASCADE",
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    // drops column
    await queryRunner.dropColumn("transactions", "user_id");
    // drops foreing key
    await queryRunner.dropForeignKey("transactions", "TransactionUserId");
  }
}
