import { IGetUserAddressDTO } from "modules/users/dtos/address/IGetUserAddressDTO";
import { Address } from "modules/users/infra/typeorm/entities/Address";
import { IAddressRepository } from "modules/users/repositories/address/IAddressRepository";
import { inject, injectable } from "tsyringe";

@injectable()
class GetUserAddressUseCase {
  private repository: IAddressRepository;

  constructor(
    @inject("AddressRepository")
    repository: IAddressRepository
  ) {
    Object.assign(this, { repository });
  }

  async execute({ user_id }: IGetUserAddressDTO): Promise<Address> {
    const address = await this.repository.findByUserId(user_id);

    return address;
  }
}

export { GetUserAddressUseCase };
