import { Request, Response } from "express";
import { container } from "tsyringe";

import { GetUserAddressUseCase } from "./GetUserAddressUseCase";

class GetUserAddressController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { id: user_id } = request.user;

    const getUserAddressUseCase = container.resolve(GetUserAddressUseCase);

    const address = await getUserAddressUseCase.execute({ user_id });

    return response.status(200).json({ address });
  }
}

export { GetUserAddressController };
