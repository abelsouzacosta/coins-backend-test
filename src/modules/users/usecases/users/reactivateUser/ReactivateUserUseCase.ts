import { IReactivateUserDTO } from "modules/users/dtos/users/IReactivateUserDTO";
import { IUserRepository } from "modules/users/repositories/users/IUserRepository";
import { IPasswordHandler } from "modules/users/utils/cryptography/password/IPasswordHandler";
import { ApplicationError } from "shared/errors/ApplicationError";
import { inject, injectable } from "tsyringe";

@injectable()
class ReactivateUseruseCase {
  private repository: IUserRepository;
  private passwordHandler: IPasswordHandler;

  constructor(
    @inject("UserRepository")
    repository: IUserRepository,
    @inject("PasswordHandler")
    passwordHandler: IPasswordHandler
  ) {
    Object.assign(this, { repository, passwordHandler });
  }

  async execute({ email, password }: IReactivateUserDTO): Promise<void> {
    const user = await this.repository.findByEmail(email);

    if (!user) throw new ApplicationError("User not found", 404);

    if (user.is_active) throw new ApplicationError("User already active", 400);

    const isPasswordValid = await this.passwordHandler.passwordCompare(
      password,
      user.password
    );

    if (!isPasswordValid) throw new ApplicationError("Invalid password", 401);

    await this.repository.reactivate(email);
  }
}

export { ReactivateUseruseCase };
