import { Request, Response } from "express";
import { container } from "tsyringe";

import { ReactivateUseruseCase } from "./ReactivateUserUseCase";

class ReactivateUserController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { email, password } = request.body;

    const reactivateUserUseCase = container.resolve(ReactivateUseruseCase);

    await reactivateUserUseCase.execute({ email, password });

    return response.status(200).send();
  }
}

export { ReactivateUserController };
