import { ICreateAddressDTO } from "modules/users/dtos/address/ICreateAddressDTO";
import { IAddressRepository } from "modules/users/repositories/address/IAddressRepository";
import { IUserRepository } from "modules/users/repositories/users/IUserRepository";
import { IAddressValidator } from "modules/users/utils/address/validator/IAddressValidator";
import { ApplicationError } from "shared/errors/ApplicationError";
import { inject, injectable } from "tsyringe";

@injectable()
class CreateAddressUseCase {
  private repository: IAddressRepository;
  private userRepository: IUserRepository;
  private addressValidator: IAddressValidator;

  constructor(
    @inject("AddressRepository")
    repository: IAddressRepository,
    @inject("UserRepository")
    userRepository: IUserRepository,
    @inject("AddressValidator")
    addressValidator: IAddressValidator
  ) {
    Object.assign(this, { repository, userRepository, addressValidator });
  }

  async execute({
    street,
    number,
    district,
    city,
    zipcode,
    user_id,
  }: ICreateAddressDTO): Promise<void> {
    const user = await this.userRepository.findById(user_id);

    if (!user) throw new ApplicationError("User not found", 404);

    const userAlreadyHasAnAddress = await this.repository.findByUserId(user_id);

    await this.addressValidator.validate(zipcode, city, street);

    if (userAlreadyHasAnAddress)
      throw new ApplicationError(
        "The user already has an registered address",
        409
      );

    await this.repository.create({
      street,
      number,
      district,
      city,
      zipcode,
      user_id,
    });
  }
}

export { CreateAddressUseCase };
