import { Request, Response } from "express";
import { UserValidator } from "modules/users/utils/users/implementation/UserValidator";
import { container } from "tsyringe";

import { CreateAddressUseCase } from "./CreateAddressUseCase";

class CreateAddressController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { street, number, district, city, zipcode, user_id } = request.body;
    const { id } = request.user;

    const createAddressUseCase = container.resolve(CreateAddressUseCase);

    const userValidator = container.resolve(UserValidator);

    await userValidator.validate(user_id, id);

    await createAddressUseCase.execute({
      street,
      number,
      district,
      city,
      zipcode,
      user_id,
    });

    return response.status(201).send();
  }
}

export { CreateAddressController };
