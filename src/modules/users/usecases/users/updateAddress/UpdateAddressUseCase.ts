import { IUpdateAddressDTO } from "modules/users/dtos/address/IUpdateAddressDTO";
import { IUpdateAddressRequestDTO } from "modules/users/dtos/address/IUpdateAddressRequestDTO";
import { IAddressRepository } from "modules/users/repositories/address/IAddressRepository";
import { IUserRepository } from "modules/users/repositories/users/IUserRepository";
import { IAddressValidator } from "modules/users/utils/address/validator/IAddressValidator";
import { ApplicationError } from "shared/errors/ApplicationError";
import { inject, injectable } from "tsyringe";

@injectable()
class UpdateAddressUseCase {
  private repository: IAddressRepository;
  private userRepository: IUserRepository;
  private addressValidator: IAddressValidator;

  constructor(
    @inject("AddressRepository")
    repository: IAddressRepository,
    @inject("UserRepository")
    userRepository: IUserRepository,
    @inject("AddressValidator")
    addressValidator: IAddressValidator
  ) {
    Object.assign(this, { repository, userRepository, addressValidator });
  }

  async execute({
    id,
    street,
    number,
    district,
    city,
    zipcode,
  }: IUpdateAddressDTO): Promise<void> {
    const address = await this.repository.findById(id);

    if (!address) throw new ApplicationError("Address not found", 404);

    const zipcodeValidationParam = zipcode || address.zipcode;
    const cityValidationParam = city || address.city;
    const streetValidationParam = street || address.street;

    await this.addressValidator.validate(
      zipcodeValidationParam,
      cityValidationParam,
      streetValidationParam
    );

    this.repository.update({ id, street, number, district, city, zipcode });
  }
}

export { UpdateAddressUseCase };
