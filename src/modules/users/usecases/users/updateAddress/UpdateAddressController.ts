import { Request, Response } from "express";
import { container } from "tsyringe";

import { UpdateAddressUseCase } from "./UpdateAddressUseCase";

class UpdateAddressController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { id } = request.params;
    const { street, number, district, city, zipcode } = request.body;

    const updateAddressUseCase = container.resolve(UpdateAddressUseCase);

    await updateAddressUseCase.execute({
      id,
      street,
      number,
      district,
      city,
      zipcode,
    });

    return response.status(200).send();
  }
}

export { UpdateAddressController };
