import { ICreateUserDTO } from "modules/users/dtos/users/ICreateUserDTO";
import { IUserRepository } from "modules/users/repositories/users/IUserRepository";
import { IPasswordHandler } from "modules/users/utils/cryptography/password/IPasswordHandler";
import { ApplicationError } from "shared/errors/ApplicationError";
import { inject, injectable } from "tsyringe";

@injectable()
class CreateUserUseCase {
  private repository: IUserRepository;
  private passwordHandler: IPasswordHandler;

  constructor(
    @inject("UserRepository")
    repository: IUserRepository,
    @inject("PasswordHandler")
    passwordHandler: IPasswordHandler
  ) {
    Object.assign(this, { repository, passwordHandler });
  }

  async execute({
    name,
    email,
    password,
    balance,
    is_active,
  }: ICreateUserDTO): Promise<void> {
    const emailAlreadyTaken = await this.repository.findByEmail(email);

    if (emailAlreadyTaken)
      throw new ApplicationError("Email already taken", 409);

    const hashedPassword = await this.passwordHandler.passwordHash(password, 8);

    await this.repository.create({
      name,
      email,
      password: hashedPassword,
      balance,
      is_active,
    });
  }
}

export { CreateUserUseCase };
