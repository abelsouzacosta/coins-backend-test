import { IDeleteAddressDTO } from "modules/users/dtos/address/IDeleteAddressDTO";
import { IAddressRepository } from "modules/users/repositories/address/IAddressRepository";
import { ApplicationError } from "shared/errors/ApplicationError";
import { inject, injectable } from "tsyringe";

@injectable()
class DeleteAddressUseCase {
  private repository: IAddressRepository;

  constructor(
    @inject("AddressRepository")
    repository: IAddressRepository
  ) {
    Object.assign(this, { repository });
  }

  async execute({ id }: IDeleteAddressDTO): Promise<void> {
    const address = await this.repository.findById(id);

    if (!address) throw new ApplicationError("Address not found", 404);

    await this.repository.delete({ id });
  }
}

export { DeleteAddressUseCase };
