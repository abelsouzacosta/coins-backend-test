import { IDisableUserDTO } from "modules/users/dtos/users/IDisableUserDTO";
import { IUserRepository } from "modules/users/repositories/users/IUserRepository";
import { ApplicationError } from "shared/errors/ApplicationError";
import { inject, injectable } from "tsyringe";

@injectable()
class DisableUserUseCase {
  private repository: IUserRepository;

  constructor(
    @inject("UserRepository")
    repository: IUserRepository
  ) {
    Object.assign(this, { repository });
  }

  async execute({ id }: IDisableUserDTO): Promise<void> {
    const userExists = await this.repository.findById(id);

    if (!userExists) throw new ApplicationError("User not found", 404);

    if (!userExists.is_active)
      throw new ApplicationError("User already disabled", 400);

    await this.repository.disable({ id });
  }
}

export { DisableUserUseCase };
