import { Request, Response } from "express";
import { container } from "tsyringe";

import { DisableUserUseCase } from "./DisableUserUseCase";

class DisableUserController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { id } = request.user;

    const disableUserUseCase = container.resolve(DisableUserUseCase);

    await disableUserUseCase.execute({ id });

    return response.status(200).send();
  }
}

export { DisableUserController };
