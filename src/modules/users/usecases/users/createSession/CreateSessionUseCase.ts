import authConfig from "config/auth";
import { sign } from "jsonwebtoken";
import { ICreateSessionDTO } from "modules/users/dtos/sessions/ICreateSessionDTO";
import { IUserRepository } from "modules/users/repositories/users/IUserRepository";
import { IPasswordHandler } from "modules/users/utils/cryptography/password/IPasswordHandler";
import { ApplicationError } from "shared/errors/ApplicationError";
import { inject, injectable } from "tsyringe";

@injectable()
class CreateSessionUseCase {
  private repository: IUserRepository;
  private passwordHandler: IPasswordHandler;

  constructor(
    @inject("UserRepository")
    repository: IUserRepository,
    @inject("PasswordHandler")
    passwordHandler: IPasswordHandler
  ) {
    Object.assign(this, { repository, passwordHandler });
  }

  async execute({ email, password }: ICreateSessionDTO): Promise<string> {
    const user = await this.repository.findByEmail(email);

    if (!user) throw new ApplicationError("user not found", 404);

    if (!user.is_active)
      throw new ApplicationError("User must be active to be logged in", 400);

    const validPassword = await this.passwordHandler.passwordCompare(
      password,
      user.password
    );

    if (!validPassword) throw new ApplicationError("Incorrect password", 401);

    const token = sign({}, String(authConfig.jwt.secret), {
      expiresIn: 86400,
      subject: user.id,
    });

    return token;
  }
}

export { CreateSessionUseCase };
