interface IUserValidator {
  validate(user_id: string, id: string): Promise<void>;
}

export { IUserValidator };
