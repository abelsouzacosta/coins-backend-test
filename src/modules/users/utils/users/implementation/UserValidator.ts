import { IUserRepository } from "modules/users/repositories/users/IUserRepository";
import { ApplicationError } from "shared/errors/ApplicationError";
import { inject, injectable } from "tsyringe";

import { IUserValidator } from "../validator/IUserValidator";

@injectable()
class UserValidator implements IUserValidator {
  private repository: IUserRepository;

  constructor(
    @inject("UserRepository")
    repository: IUserRepository
  ) {
    Object.assign(this, { repository });
  }

  async validate(user_id: string, id: string): Promise<void> {
    const userByRequest = await this.repository.findById(user_id);
    const userById = await this.repository.findById(id);

    if (userByRequest !== userById)
      throw new ApplicationError("Invalid argument user_id", 412);
  }
}

export { UserValidator };
