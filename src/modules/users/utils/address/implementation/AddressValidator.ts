import axios from "axios";
import { ApplicationError } from "shared/errors/ApplicationError";

import { IAddressValidator } from "../validator/IAddressValidator";

class AddressValidator implements IAddressValidator {
  async validate(zipcode: string, city: string, street: string): Promise<void> {
    const url = `${zipcode}/json`;

    const response = await axios(url, {
      baseURL: `https://viacep.com.br/ws/`,
    });

    const { localidade, logradouro } = response.data;

    const cityResponseToUpperCase = localidade.toUpperCase();
    const cityToUpperCase = city.toUpperCase();
    const streetResponseToUpperCase = logradouro.toUpperCase();
    const streetToUpperCase = street.toUpperCase();

    if (
      cityToUpperCase !== cityResponseToUpperCase ||
      streetResponseToUpperCase !== streetToUpperCase
    )
      throw new ApplicationError(
        "Invalid address, city is not compatible with zipcode given",
        400
      );
  }
}

export { AddressValidator };
