interface IAddressValidator {
  validate(zipcode: string, city: string, street: string): Promise<void>;
}

export { IAddressValidator };
