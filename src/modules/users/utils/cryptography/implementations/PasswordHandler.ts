import { hash, compare } from "bcrypt";

import { IPasswordHandler } from "../password/IPasswordHandler";

class PasswordHandler implements IPasswordHandler {
  async passwordHash(password: string, rounds: number): Promise<string> {
    const hashedPassword = await hash(password, rounds);

    return hashedPassword;
  }

  async passwordCompare(password: string, hash: string): Promise<boolean> {
    const passwordIsValid = await compare(password, hash);

    return passwordIsValid;
  }
}

export { PasswordHandler };
