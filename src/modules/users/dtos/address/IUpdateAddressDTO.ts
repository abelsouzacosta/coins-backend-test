interface IUpdateAddressDTO {
  id: string;
  street: string;
  number: number;
  district: string;
  city: string;
  zipcode: string;
}

export { IUpdateAddressDTO };
