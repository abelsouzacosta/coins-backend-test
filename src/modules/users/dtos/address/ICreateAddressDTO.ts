interface ICreateAddressDTO {
  street: string;
  number: number;
  district: string;
  city: string;
  zipcode: string;
  user_id: string;
}

export { ICreateAddressDTO };
