interface IReactivateUserDTO {
  email: string;
  password: string;
}

export { IReactivateUserDTO };
