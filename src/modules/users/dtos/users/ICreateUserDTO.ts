interface ICreateUserDTO {
  name: string;
  email: string;
  password: string;
  balance: number;
  is_active: boolean;
}

export { ICreateUserDTO };
