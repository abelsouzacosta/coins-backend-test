import { ICreateAddressDTO } from "modules/users/dtos/address/ICreateAddressDTO";
import { IDeleteAddressDTO } from "modules/users/dtos/address/IDeleteAddressDTO";
import { IUpdateAddressDTO } from "modules/users/dtos/address/IUpdateAddressDTO";
import { Address } from "modules/users/infra/typeorm/entities/Address";

interface IAddressRepository {
  findById(id: string): Promise<Address | undefined>;

  findByUserId(user_id: string): Promise<Address | undefined>;

  create({
    street,
    number,
    district,
    city,
    zipcode,
    user_id,
  }: ICreateAddressDTO): Promise<void>;

  update({
    id,
    street,
    number,
    district,
    city,
    zipcode,
  }: IUpdateAddressDTO): Promise<void>;

  delete({ id }: IDeleteAddressDTO): Promise<void>;
}

export { IAddressRepository };
