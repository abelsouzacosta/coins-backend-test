import { ICreateUserDTO } from "modules/users/dtos/users/ICreateUserDTO";
import { IDisableUserDTO } from "modules/users/dtos/users/IDisableUserDTO";
import { User } from "modules/users/infra/typeorm/entities/User";

interface IUserRepository {
  findByEmail(email: string): Promise<User | undefined>;

  findById(id: string): Promise<User | undefined>;

  create({
    name,
    email,
    password,
    balance,
    is_active,
  }: ICreateUserDTO): Promise<void>;

  disable({ id }: IDisableUserDTO): Promise<void>;

  reactivate(email: string): Promise<void>;

  deposit(id: string, amount: number): Promise<void>;

  cashOutFlow(id: string, amount: number): Promise<void>;
}

export { IUserRepository };
