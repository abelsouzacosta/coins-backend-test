import {
  Column,
  CreateDateColumn,
  Entity,
  OneToOne,
  PrimaryColumn,
} from "typeorm";
import { v4 as uuidv4 } from "uuid";

import { User } from "./User";

@Entity("address")
class Address {
  @PrimaryColumn()
  id?: string;

  @Column()
  street: string;

  @Column()
  number: number;

  @Column()
  district: string;

  @Column()
  city: string;

  @Column()
  zipcode: string;

  @CreateDateColumn()
  created_at: Date;

  @Column()
  user_id: string;

  @OneToOne(() => User, (user) => user.address)
  user: User;

  constructor() {
    this.id = this.id ? this.id : uuidv4();
  }
}

export { Address };
