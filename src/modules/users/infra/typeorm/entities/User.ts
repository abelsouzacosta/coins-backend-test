import { Transaction } from "modules/transactions/infra/typeorm/entities/Transaction";
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  Transaction,
} from "typeorm";
import { v4 as uuidv4 } from "uuid";

import { Address } from "./Address";

@Entity("users")
class User {
  @PrimaryColumn()
  id?: string;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column()
  balance: number;

  @Column()
  is_active: boolean;

  @CreateDateColumn()
  created_at: Date;

  @OneToOne(() => Address, (address) => address.user)
  address: Address;

  @OneToMany(() => Transaction, (transaction) => transaction.user, {
    cascade: true,
  })
  transactions: Transaction[];

  constructor() {
    this.id = this.id ? this.id : uuidv4();
  }
}

export { User };
