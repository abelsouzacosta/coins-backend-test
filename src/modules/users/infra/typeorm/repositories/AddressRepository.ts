import { ICreateAddressDTO } from "modules/users/dtos/address/ICreateAddressDTO";
import { IDeleteAddressDTO } from "modules/users/dtos/address/IDeleteAddressDTO";
import { IUpdateAddressDTO } from "modules/users/dtos/address/IUpdateAddressDTO";
import { IAddressRepository } from "modules/users/repositories/address/IAddressRepository";
import { getRepository, Repository } from "typeorm";

import { Address } from "../entities/Address";

class AddressRepository implements IAddressRepository {
  private repository: Repository<Address>;

  constructor() {
    this.repository = getRepository(Address);
  }

  async findByUserId(user_id: string): Promise<Address | undefined> {
    const address = await this.repository.findOne({
      where: {
        user_id,
      },
    });

    return address;
  }

  async findById(id: string): Promise<Address> {
    const address = await this.repository.findOne({
      where: {
        id,
      },
    });

    return address;
  }

  async create({
    street,
    number,
    district,
    city,
    zipcode,
    user_id,
  }: ICreateAddressDTO): Promise<void> {
    const address = this.repository.create({
      street,
      number,
      district,
      city,
      zipcode,
      user_id,
    });

    await this.repository.save(address);
  }

  async update({
    id,
    street,
    number,
    district,
    city,
    zipcode,
  }: IUpdateAddressDTO): Promise<void> {
    const address = await this.findById(id);

    if (address) {
      address.street = street || address.street;
      address.number = number || address.number;
      address.district = district || address.district;
      address.city = city || address.city;
      address.zipcode = zipcode || address.zipcode;
      await this.repository.save(address);
    }
  }

  async delete({ id }: IDeleteAddressDTO): Promise<void> {
    const address = await this.findById(id);

    if (address) this.repository.remove(address);
  }
}

export { AddressRepository };
