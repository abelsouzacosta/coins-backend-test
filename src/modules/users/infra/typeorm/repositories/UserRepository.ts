import { ICreateUserDTO } from "modules/users/dtos/users/ICreateUserDTO";
import { IDisableUserDTO } from "modules/users/dtos/users/IDisableUserDTO";
import { IUserRepository } from "modules/users/repositories/users/IUserRepository";
import { getRepository, Repository } from "typeorm";

import { User } from "../entities/User";

class UserRepository implements IUserRepository {
  private repository: Repository<User>;

  constructor() {
    this.repository = getRepository(User);
  }

  async findByEmail(email: string): Promise<User | undefined> {
    const user = await this.repository.findOne({
      where: {
        email,
      },
    });

    return user;
  }

  async findById(id: string): Promise<User> {
    const user = await this.repository.findOne({
      where: {
        id,
      },
    });

    return user;
  }

  async create({
    name,
    email,
    password,
    balance,
    is_active,
  }: ICreateUserDTO): Promise<void> {
    const user = this.repository.create({
      name,
      email,
      password,
      balance,
      is_active,
    });

    await this.repository.save(user);
  }

  async disable({ id }: IDisableUserDTO): Promise<void> {
    const user = await this.findById(id);

    if (user) {
      user.is_active = false;
      await this.repository.save(user);
    }
  }

  async reactivate(email: string): Promise<void> {
    const user = await this.findByEmail(email);

    if (user) {
      user.is_active = true;
      await this.repository.save(user);
    }
  }

  async deposit(id: string, newBalance: number): Promise<void> {
    const user = await this.findById(id);

    if (user) {
      user.balance = newBalance;
      await this.repository.save(user);
    }
  }

  async cashOutFlow(id: string, newBalance: number): Promise<void> {
    const user = await this.findById(id);

    if (user) {
      user.balance = newBalance;
      await this.repository.save(user);
    }
  }
}

export { UserRepository };
