interface ICreateTransactionDTO {
  type: string;
  amount: number;
  user_id: string;
}

export { ICreateTransactionDTO };
