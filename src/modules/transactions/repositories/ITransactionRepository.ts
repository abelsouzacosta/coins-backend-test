import { ICreateTransactionDTO } from "../dtos/ICreateTransactionDTO";
import { Transaction } from "../infra/typeorm/entities/Transaction";

interface ITransactionRepository {
  create({ type, amount, user_id }: ICreateTransactionDTO): Promise<void>;

  findByUserId(user_id: string): Promise<Transaction[]>;
}

export { ITransactionRepository };
