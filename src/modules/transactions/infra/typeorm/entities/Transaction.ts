import { User } from "modules/users/infra/typeorm/entities/User";
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
} from "typeorm";
import { v4 as uuidv4 } from "uuid";

@Entity("transactions")
class Transaction {
  @PrimaryColumn()
  id?: string;

  @Column()
  type: string;

  @Column()
  amount: number;

  @Column()
  user_id: string;

  @CreateDateColumn()
  created_at: Date;

  @ManyToOne(() => User)
  @JoinColumn({ name: "user_id" })
  user: User;

  constructor() {
    this.id = this.id ? this.id : uuidv4();
  }
}

export { Transaction };
