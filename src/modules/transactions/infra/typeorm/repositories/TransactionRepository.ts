import { ICreateTransactionDTO } from "modules/transactions/dtos/ICreateTransactionDTO";
import { ITransactionRepository } from "modules/transactions/repositories/ITransactionRepository";
import { getRepository, Repository } from "typeorm";

import { Transaction } from "../entities/Transaction";

class TransactionRepository implements ITransactionRepository {
  private repository: Repository<Transaction>;

  constructor() {
    this.repository = getRepository(Transaction);
  }

  async create({
    type,
    amount,
    user_id,
  }: ICreateTransactionDTO): Promise<void> {
    const transaction = this.repository.create({
      type,
      amount,
      user_id,
    });

    await this.repository.save(transaction);
  }

  async findByUserId(user_id: string): Promise<Transaction[]> {
    const transaction = await this.repository.find({
      where: {
        user_id,
      },
    });

    return transaction;
  }
}

export { TransactionRepository };
