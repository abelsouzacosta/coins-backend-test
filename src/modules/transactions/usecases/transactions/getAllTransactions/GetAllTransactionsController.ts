import { Request, Response } from "express";
import { container } from "tsyringe";

import { GetAllTransactionsUseCase } from "./GetAllTransactionsUseCase";

class GetAllTransactionsController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { id } = request.user;

    const getAllTransactionsUseCase = container.resolve(
      GetAllTransactionsUseCase
    );

    const transactions = await getAllTransactionsUseCase.execute(id);

    return response.status(200).json({
      transactions,
    });
  }
}

export { GetAllTransactionsController };
