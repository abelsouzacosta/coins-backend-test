import { Transaction } from "modules/transactions/infra/typeorm/entities/Transaction";
import { ITransactionRepository } from "modules/transactions/repositories/ITransactionRepository";
import { ApplicationError } from "shared/errors/ApplicationError";
import { inject, injectable } from "tsyringe";

@injectable()
class GetAllTransactionsUseCase {
  private repository: ITransactionRepository;

  constructor(
    @inject("TransactionRepository")
    repository: ITransactionRepository
  ) {
    Object.assign(this, { repository });
  }

  async execute(user_id: string): Promise<Transaction[]> {
    const transactions = await this.repository.findByUserId(user_id);

    if (!transactions)
      throw new ApplicationError("Any transaction was found", 404);

    return transactions;
  }
}

export { GetAllTransactionsUseCase };
