import { ICreateTransactionDTO } from "modules/transactions/dtos/ICreateTransactionDTO";
import { ITransactionRepository } from "modules/transactions/repositories/ITransactionRepository";
import { IUserRepository } from "modules/users/repositories/users/IUserRepository";
import { ApplicationError } from "shared/errors/ApplicationError";
import { inject, injectable } from "tsyringe";

@injectable()
class CreateTransactionUseCase {
  private repository: ITransactionRepository;
  private userRepository: IUserRepository;

  constructor(
    @inject("TransactionRepository")
    repository: ITransactionRepository,
    @inject("UserRepository")
    userRepository: IUserRepository
  ) {
    Object.assign(this, { repository, userRepository });
  }

  async execute({
    type,
    amount,
    user_id,
  }: ICreateTransactionDTO): Promise<void> {
    const user = await this.userRepository.findById(user_id);

    if (!user) throw new ApplicationError("User not found", 404);

    if (!user.is_active) throw new ApplicationError("User is not active", 400);

    const transactionTypes = ["purchase", "deposit", "withdraw"];

    if (!transactionTypes.includes(type))
      throw new ApplicationError("Invalid transaction type", 402);

    if ((type === "purchase" || type === "withdraw") && amount > user.balance)
      throw new ApplicationError("Amount higher than user balance", 400);
    else {
      const newBalance = user.balance - amount;

      await this.userRepository.cashOutFlow(user.id, newBalance);
    }

    if (type === "deposit") {
      const newBalance = user.balance + amount;

      await this.userRepository.deposit(user.id, newBalance);
    }

    await this.repository.create({ type, amount, user_id });
  }
}

export { CreateTransactionUseCase };
