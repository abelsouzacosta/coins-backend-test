import { Request, Response } from "express";
import { container } from "tsyringe";

import { CreateTransactionUseCase } from "./CreateTransactionUseCase";

class CreateTransactionController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { id: user_id } = request.user;
    const { type, amount } = request.body;

    const createTransactionUseCase = container.resolve(
      CreateTransactionUseCase
    );

    await createTransactionUseCase.execute({ type, amount, user_id });

    return response.status(200).send();
  }
}

export { CreateTransactionController };
