import "reflect-metadata";
import express from "express";
import "express-async-errors";

import "./shared/infra/typeorm";
import "./shared/container";
import errorHandler from "./shared/infra/middlewares/ErrorHandler";
import { router } from "./shared/routes";

const app = express();

app.use(express.json());
app.use(router);
app.use(errorHandler);

app.listen(3000, () => console.log(`App listen on port 3000`));
